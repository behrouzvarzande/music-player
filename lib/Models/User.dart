

import 'package:avapardaz_music/Models/Channel.dart';
import 'package:avapardaz_music/Models/PlayList.dart';

class User {

 static String name ="";

 static String email ="";

 static String phone ="";

 static String accessToken ="";
 
 static List<PlayList> playList=[];

 static List<Channel> followedChannels =[];

}