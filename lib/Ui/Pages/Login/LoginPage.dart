import 'package:avapardaz_music/Models/User.dart';
import 'package:avapardaz_music/NetworkUtils/Network_Utils.dart';
import 'package:avapardaz_music/Theme/Consts.dart';
import 'package:avapardaz_music/Theme/sizeConfig.dart';
import 'package:avapardaz_music/Ui/Components/TextFieldComponen.dart';
import 'package:avapardaz_music/Ui/Components/LoginButton.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormBuilderState> formKey = GlobalKey<FormBuilderState>();
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        Navigator.pushReplacementNamed(context, '/root');
      },
          child: Scaffold(
        body: Directionality(
          textDirection: TextDirection.rtl,
          child: SafeArea(
              child: SingleChildScrollView(
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: K_primaryBlack,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  FormBuilder(
                    key: formKey,
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 60),
                          child: new Text(
                            'ورود به جانا',
                            style: TextStyle(
                                color: K_LightPurpule,
                                fontSize: 20,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        LoginButton(
                          onPressed: (){},
                            title: "ورود با حساب گوگل",
                            width: double.infinity,
                            height: 50,
                            borderRadius: 10,
                            gradient: false,
                            backGroundColor: Colors.white,
                            textStyle:
                                TextStyle(color: Colors.black, fontSize: 14),
                            icon: Image(
                              image: AssetImage('assets/images/google.png'),
                              height: 30,
                              width: 30,
                            ),
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            textDirection: TextDirection.rtl,
                            active: true),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 20),
                          child: Row(children: <Widget>[
                            Expanded(
                              child: new Container(
                                  margin: const EdgeInsets.only(
                                      left: 10.0, right: 30.0),
                                  child: Divider(
                                    color: K_primaryWhite,
                                    height: 4.61 * SizeConfig.heightMultiplier,
                                  )),
                            ),
                            Text(
                              "یا",
                              style: TextStyle(
                                  color: K_primaryWhite,
                                  fontSize: 1.7 * SizeConfig.textMultiplier),
                            ),
                            Expanded(
                              child: new Container(
                                  margin: const EdgeInsets.only(
                                      left: 30.0, right: 10.0),
                                  child: Divider(
                                    color: K_primaryWhite,
                                    height: 4.61 * SizeConfig.heightMultiplier,
                                  )),
                            ),
                          ]),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: TextFieldComponent(
                            attribute: 'email',
                            hintText: '',
                            labelText: 'ایمیل',
                            requiredField: false,
                            needChecking: false,
                            validators: [
                              FormBuilderValidators.email(
                                  errorText: 'ایمیل نامعتبر')
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 20),
                          child: TextFieldComponent(
                            attribute: 'password',
                            hintText: '',
                            labelText: 'کلمه عبور',
                            requiredField: false,
                            needChecking: false,
                          ),
                        ),
                        LoginButton(
                          title: "ورود",
                          gradient: true,
                          width: double.infinity,
                          height: 50,
                          borderRadius: 10,
                          backGroundColor: K_LightPurpule,
                          textStyle: TextStyle(color: Colors.white, fontSize: 14),
                          icon: null,
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          textDirection: TextDirection.rtl,
                          active: true,
                          onPressed: () async{
                            if (formKey.currentState.saveAndValidate()) {
                              print(formKey.currentState.value);
                              if(await NetworkUtil.internal().postLogin(formKey.currentState.value)){
                                if(await NetworkUtil.internal().getUserInfo(User.accessToken)){
                                  Navigator.pushReplacementNamed(context, "/root");
                                }
                              }
                            }
                          },
                        ),
                      ],
                    ),
                  ),
                  Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 10),
                        child: GestureDetector(
                          onTap: () {
                            Navigator.pushNamed(context, '/forgot1');
                          },
                          child: Text(
                            'کلمه عبور خود را فراموش کردید؟',
                            style: TextStyle(
                              color: K_accentblackLighter,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 20),
                        child: GestureDetector(
                          onTap: () {
                            Navigator.pushNamed(context, '/signUp');
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'حساب کاربری ندارید؟',
                                style: TextStyle(
                                  color: K_accentblackLighter,
                                ),
                              ),
                              Text(
                                ' ثبت نام',
                                style: TextStyle(
                                    color: K_accentblackLighter, fontSize: 16),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          )),
        ),
      ),
    );
  }
}
