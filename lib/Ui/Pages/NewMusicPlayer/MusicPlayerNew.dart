import 'dart:async';
import 'dart:math';
import 'package:audio_service/audio_service.dart';
import 'package:avapardaz_music/Blocs/PlayBloc.dart';
import 'package:avapardaz_music/Theme/Consts.dart';
import 'package:avapardaz_music/Theme/sizeConfig.dart';
import 'package:avapardaz_music/Ui/Components/MoreOptionsModel.dart';
import 'package:avapardaz_music/Ui/Pages/NewMusicPlayer/AudioPlayerTask.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:lottie/lottie.dart';
import 'package:rxdart/rxdart.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:avapardaz_music/main.dart';
import 'package:provider/provider.dart';

MediaControl playControl = MediaControl(
  androidIcon: 'drawable/ic_action_play_arrow',
  label: 'Play',
  action: MediaAction.play,
);
MediaControl pauseControl = MediaControl(
  androidIcon: 'drawable/ic_action_pause',
  label: 'Pause',
  action: MediaAction.pause,
);
MediaControl skipToNextControl = MediaControl(
  androidIcon: 'drawable/ic_action_skip_next',
  label: 'Next',
  action: MediaAction.skipToNext,
);
MediaControl skipToPreviousControl = MediaControl(
  androidIcon: 'drawable/ic_action_skip_previous',
  label: 'Previous',
  action: MediaAction.skipToPrevious,
);
MediaControl stopControl = MediaControl(
  androidIcon: 'drawable/ic_action_stop',
  label: 'Stop',
  action: MediaAction.stop,
);

class NewMusicPlayer extends StatefulWidget {
  @override
  _NewMusicPlayerState createState() => _NewMusicPlayerState();

  final ScrollController sc;
  final PanelController panelController;

  static audioPlayerButton() async {
    List<dynamic> list = List();
    for (int i = 0; i < queueBackGround.length; i++) {
      var m = queueBackGround[i].toJson();
      list.add(m);
    }
    var params = {'data': list};
    // var a = queueBackGround.length;
    await AudioService.start(
      backgroundTaskEntrypoint: _audioPlayerTaskEntrypoint,
      androidNotificationChannelName: 'Audio Player',
      androidNotificationColor: 0xFF2196f3,
      androidNotificationIcon: 'mipmap/ic_launcher',
      params: params,
    );
  }

  NewMusicPlayer({@required this.panelController, @required this.sc});
}

class _NewMusicPlayerState extends State<NewMusicPlayer> {
  final BehaviorSubject<double> _dragPositionSubject =
      BehaviorSubject.seeded(null);

  bool firstTime;

  double _panelHeightOpen;

  double _panelWidthOpen;

  @override
  void initState() {
    super.initState();
    firstTime = true;
    // audioPlayerButton();
  }

  @override
  void dispose() {
    AudioService.stop();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _panelHeightOpen = MediaQuery.of(context).size.height;
    _panelWidthOpen = MediaQuery.of(context).size.width;
    return StreamBuilder<ScreenState>(
      stream: _screenStateStream,
      builder: (context, snapshot) {
        final screenState = snapshot.data;
        final queue = screenState?.queue;
        final mediaItem = screenState?.mediaItem;
        final state = screenState?.playbackState;
        final processingState =
            state?.processingState ?? AudioProcessingState.none;
        final playing = state?.playing ?? false;
        return SingleChildScrollView(
          controller: widget.sc,
          child: Container(
            color: K_primaryBlack,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                if (processingState == AudioProcessingState.none) ...[
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: IconButton(
                      icon: SvgPicture.asset(
                        'assets/images/Line.svg',
                        semanticsLabel: 'minus',
                        color: K_transparentWhite,
                        height: 200,
                        width: 200,
                      ),
                      onPressed: () {
                        widget.panelController.close();
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: Container(
                      width: _panelWidthOpen * 60 / 100,
                      height: _panelWidthOpen * 60 / 100,
                      margin: EdgeInsets.only(
                          bottom: _panelHeightOpen / 20,
                          top: _panelWidthOpen / 15),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          fit: BoxFit.fill,
                          image: NetworkImage(
                              queueBackGround[songsPlayIndex].artUri),
                        ),
                      ),
                    ),
                  ),

                  //  Padding(
                  //    padding: EdgeInsets.symmetric(horizontal: 16.0),
                  //    child: queue != null && queue.isNotEmpty ? positionIndicator(mediaItem,state) : new Text(''),
                  //  ),

                  //!--------------------------------------------------------
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: Container(
                      margin: EdgeInsets.only(top: 32),
                      child: Text(
                        queueBackGround[songsPlayIndex].title,
                        style: TextStyle(
                          fontSize: 18,
                          color: K_primaryWhite,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: Container(
                      margin: EdgeInsets.symmetric(vertical: 8),
                      child: Text(
                        queueBackGround[songsPlayIndex].artist,
                        style: TextStyle(
                          fontSize: 12,
                          color: K_primaryWhite,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Material(
                          color: Colors.transparent,
                          child: IconButton(
                            icon: SvgPicture.asset(
                              'assets/images/like.svg',
                              semanticsLabel: 'like',
                              color: K_transparentWhite,
                              height: 24,
                              width: 24,
                            ),
                            onPressed: () {},
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(
                              width: 54,
                              height: 54,
                              child: FlatButton(
                                  padding: EdgeInsets.all(0),
                                  color: Colors.transparent,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(90.0),
                                  ),
                                  onPressed: () {
                                    if (mediaItem == queue.first) {
                                      setState(() {
                                        // songsPlayIndex=queueBackGround.length-1;
                                      });
                                      return;
                                    }
                                    setState(() {
                                      // songsPlayIndex--;
                                      AudioService.skipToPrevious();
                                    });
                                  },
                                  child: Container(
                                    height: 16,
                                    width: 16,
                                    child: SvgPicture.asset(
                                      'assets/images/previous.svg',
                                      semanticsLabel: 'pause',
                                      color: K_transparentWhite,
                                    ),
                                  )),
                            ),
                            SizedBox(
                              width: 54,
                              height: 54,
                              child: playButtonFIrst(),
                            ),
                            SizedBox(
                              width: 54,
                              height: 54,
                              child: FlatButton(
                                  padding: EdgeInsets.all(0),
                                  color: Colors.transparent,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(90.0),
                                  ),
                                  onPressed: () {
                                    if (mediaItem == queue.last) {
                                      setState(() {
                                        songsPlayIndex = 0;
                                      });
                                      return;
                                    }
                                    setState(() {
                                      songsPlayIndex++;
                                      AudioService.skipToNext();
                                    });
                                  },
                                  child: Container(
                                    height: 16,
                                    width: 16,
                                    child: SvgPicture.asset(
                                      'assets/images/next.svg',
                                      semanticsLabel: 'pause',
                                      color: K_transparentWhite,
                                    ),
                                  )),
                            ),
                          ],
                        ),
                        Material(
                          color: Colors.transparent,
                          child: IconButton(
                            icon: Icon(
                              Icons.bookmark_border,
                              size: 36,
                              color: K_transparentWhite2,
                            ),
                            onPressed: () {},
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: Divider(
                      height: 16,
                      color: K_transparentWhite2,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: Container(
                      width: double.infinity,
                      height: 48,
                      margin: EdgeInsets.symmetric(vertical: 16),
                      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                      child: MaterialButton(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'تصادفی',
                              style: TextStyle(
                                  fontSize: 16, color: K_primaryOrange),
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            SvgPicture.asset(
                              'assets/images/random.svg',
                              semanticsLabel: 'random',
                              color: K_primaryOrange,
                              height: 14,
                              width: 14,
                            ),
                          ],
                        ),
                        color: K_transparentWhite2,
                        onPressed: () {
                          //  AudioService.stop();
                        },
                      ),
                    ),
                  ),
                  Selector<PlayBloc, List<MediaItem>>(
                    selector: (_, bloc) => bloc.medias,
                    builder: (context, medias, child) => Column(
                      children: [
                        for (var i = 0; i < medias.length; i++)
                          Directionality(
                            textDirection: TextDirection.rtl,
                            child: Padding(
                              padding: EdgeInsets.only(right: 10),
                              child: Column(
                                children: [
                                  new ListTile(
                                    leading: new CircleAvatar(
                                      radius: 25,
                                      backgroundImage:
                                          new NetworkImage(medias[i].artUri),
                                      backgroundColor: Colors.grey,
                                    ),
                                    title: new Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        SizedBox(
                                          width: 40.66 *
                                              SizeConfig.widthMultiplier,
                                          child: new Text(
                                            medias[i].title,
                                            style: new TextStyle(
                                                fontFamily: "IRANYekanMobile",
                                                fontSize: 2.34 *
                                                    SizeConfig.textMultiplier,
                                                color: K_primaryWhite,
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              left: 10, top: 10),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: [
                                              Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 5,
                                                      top: 1.46 *
                                                          SizeConfig
                                                              .heightMultiplier),
                                                  child: new Text(
                                                    "22:30",
                                                    style: new TextStyle(
                                                        fontFamily:
                                                            "IRANYekanMobile",
                                                        fontSize: 1.75 *
                                                            SizeConfig
                                                                .textMultiplier,
                                                        color:
                                                            Color(0xffF6E9E9),
                                                        fontWeight:
                                                            FontWeight.w400),
                                                  )),
                                              Padding(
                                                padding: EdgeInsets.only(
                                                  top: 1.46 *
                                                      SizeConfig
                                                          .heightMultiplier,
                                                ),
                                                child: GestureDetector(
                                                  onTap: () {
                                                    showModalBottomSheet(
                                                        context: context,
                                                        builder: (builder) {
                                                          return MoreOptions();
                                                        });
                                                  },
                                                  child: new Icon(
                                                    Icons.more_vert,
                                                    color: K_primaryWhite,
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                    subtitle: new Padding(
                                      padding: EdgeInsets.only(top: 0),
                                      child: new Text(
                                        "medias[i].artist",
                                        style: new TextStyle(
                                            fontFamily: "IRANYekanMobile",
                                            fontSize: 1.79 *
                                                SizeConfig.textMultiplier,
                                            color: Color(0xffF6E9E9),
                                            fontWeight: FontWeight.w300),
                                      ),
                                    ),
                                  ),
                                  i < medias.length - 1
                                      ? new Container(
                                          margin: EdgeInsets.only(
                                              right: 70, left: 20),
                                          height: 0.5,
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.9,
                                          color: Color(0xff4B4747),
                                        )
                                      : new SizedBox(
                                          height: 0,
                                        )
                                ],
                              ),
                            ),
                          ),
                      ],
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 48,
                    color: Color(0xff2891E1),
                    child: MaterialButton(
                      hoverColor: Color(0xff2891E1),
                      // focusColor: Color(0xff2891E1),
                      splashColor: Color(0xff2891E1),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'دنبال کردن کانال',
                            style: TextStyle(fontSize: 16, color: Colors.white),
                          ),
                        ],
                      ),
                      color: K_transparentWhite2,
                      onPressed: () {},
                    ),
                  ),
                ] else ...[
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: IconButton(
                      icon: SvgPicture.asset(
                        'assets/images/Line.svg',
                        semanticsLabel: 'minus',
                        color: K_transparentWhite,
                        height: 200,
                        width: 200,
                      ),
                      onPressed: () {
                        widget.panelController.close();
                      },
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.symmetric(horizontal: 16.0),
                      child: Stack(
                        alignment: Alignment.center,
                        children: <Widget>[
                          playing
                              ? Container(
                                  width: _panelWidthOpen * 60 / 100,
                                  height: _panelWidthOpen * 60 / 100,
                                  margin: EdgeInsets.only(
                                      bottom: _panelHeightOpen / 20,
                                      top: _panelWidthOpen / 15),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                  ),
                                  child: Lottie.asset(
                                      'assets/Lottie/PlayerAnimation.json',
                                      fit: BoxFit.fill),
                                )
                              : Text(""),
                          CircleAvatar(
                            radius: (_panelWidthOpen * 23 / 100),
                            backgroundColor: Colors.grey,
                            child: new Container(
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: NetworkImage(context
                                      .select<PlayBloc, MediaItem>(
                                          (bloc) => bloc?.currentMediaItem)
                                      .artUri),
                                ),
                              ),
                            ),
                          )
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: queue != null && queue.isNotEmpty
                        ? positionIndicator(mediaItem, state)
                        : new Text(''),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: Container(
                      margin: EdgeInsets.only(top: 32),
                      child: Text(
                        context
                            .select<PlayBloc, MediaItem>(
                                (bloc) => bloc?.currentMediaItem)
                            .title,
                        style: TextStyle(
                          fontSize: 18,
                          color: K_primaryWhite,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: Container(
                      margin: EdgeInsets.symmetric(vertical: 8),
                      child: Text(
                        context
                            .select<PlayBloc, MediaItem>(
                                (bloc) => bloc?.currentMediaItem)
                            .artist,
                        style: TextStyle(
                          fontSize: 12,
                          color: K_primaryWhite,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Material(
                          color: Colors.transparent,
                          child: IconButton(
                            icon: SvgPicture.asset(
                              'assets/images/like.svg',
                              semanticsLabel: 'like',
                              color: K_transparentWhite,
                              height: 24,
                              width: 24,
                            ),
                            onPressed: () {},
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(
                              width: 54,
                              height: 54,
                              child: FlatButton(
                                  padding: EdgeInsets.all(0),
                                  color: Colors.transparent,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(90.0),
                                  ),
                                  onPressed: () {
                                    if (mediaItem == queue.first) {
                                      setState(() {
                                        // songsPlayIndex=queueBackGround.length-1;
                                      });
                                      return;
                                    }
                                    setState(() {
                                      // songsPlayIndex--;
                                      AudioService.skipToPrevious();
                                    });
                                  },
                                  child: Container(
                                    height: 16,
                                    width: 16,
                                    child: SvgPicture.asset(
                                      'assets/images/previous.svg',
                                      semanticsLabel: 'pause',
                                      color: K_transparentWhite,
                                    ),
                                  )),
                            ),
                            SizedBox(
                              width: 54,
                              height: 54,
                              child: (playing) ? pauseButton() : playButton(),
                            ),
                            SizedBox(
                              width: 54,
                              height: 54,
                              child: FlatButton(
                                  padding: EdgeInsets.all(0),
                                  color: Colors.transparent,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(90.0),
                                  ),
                                  onPressed: () {
                                    if (mediaItem == queue.last) {
                                      setState(() {
                                        songsPlayIndex = 0;
                                      });
                                      return;
                                    }
                                    setState(() {
                                      // songsPlayIndex++;
                                      AudioService.skipToNext();
                                    });
                                  },
                                  child: Container(
                                    height: 16,
                                    width: 16,
                                    child: SvgPicture.asset(
                                      'assets/images/next.svg',
                                      semanticsLabel: 'pause',
                                      color: K_transparentWhite,
                                    ),
                                  )),
                            ),
                          ],
                        ),
                        Material(
                          color: Colors.transparent,
                          child: IconButton(
                            icon: Icon(
                              Icons.bookmark_border,
                              size: 36,
                              color: K_transparentWhite2,
                            ),
                            onPressed: () {},
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: Divider(
                      height: 16,
                      color: K_transparentWhite2,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: Container(
                      width: double.infinity,
                      height: 48,
                      margin: EdgeInsets.symmetric(vertical: 16),
                      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                      child: MaterialButton(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'تصادفی',
                              style: TextStyle(
                                  fontSize: 16, color: K_primaryOrange),
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            SvgPicture.asset(
                              'assets/images/random.svg',
                              semanticsLabel: 'random',
                              color: K_primaryOrange,
                              height: 14,
                              width: 14,
                            ),
                          ],
                        ),
                        color: K_transparentWhite2,
                        onPressed: () {
                          AudioService.stop();
                        },
                      ),
                    ),
                  ),
                  Selector<PlayBloc, List<MediaItem>>(
                    selector: (_, bloc) => bloc.medias,
                    builder: (context, medias, child) => Column(
                      children: [
                        for (var i = 0; i < medias.length; i++)
                          Directionality(
                            textDirection: TextDirection.rtl,
                            child: Padding(
                              padding: EdgeInsets.only(right: 10),
                              child: Column(
                                children: [
                                  InkWell(
                                    onTap: () {
                                      Provider.of<PlayBloc>(context,
                                              listen: false)
                                          .skipToQueuItem(medias[i].id);
                                      //TODO: ......
                                    },
                                    child: new ListTile(
                                      leading: new CircleAvatar(
                                        radius: 25,
                                        backgroundImage:
                                            new NetworkImage(medias[i].artUri),
                                        backgroundColor: Colors.grey,
                                      ),
                                      title: new Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          SizedBox(
                                            width: 40.66 *
                                                SizeConfig.widthMultiplier,
                                            child: new Text(
                                              medias[i].title,
                                              style: new TextStyle(
                                                  fontFamily: "IRANYekanMobile",
                                                  fontSize: 2.34 *
                                                      SizeConfig.textMultiplier,
                                                  color: K_primaryWhite,
                                                  fontWeight: FontWeight.w400),
                                            ),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.only(
                                                left: 10, top: 10),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: [
                                                Padding(
                                                    padding: EdgeInsets.only(
                                                        left: 5,
                                                        top: 1.46 *
                                                            SizeConfig
                                                                .heightMultiplier),
                                                    child: new Text(
                                                      "22:30",
                                                      style: new TextStyle(
                                                          fontFamily:
                                                              "IRANYekanMobile",
                                                          fontSize: 1.75 *
                                                              SizeConfig
                                                                  .textMultiplier,
                                                          color:
                                                              Color(0xffF6E9E9),
                                                          fontWeight:
                                                              FontWeight.w400),
                                                    )),
                                                Padding(
                                                  padding: EdgeInsets.only(
                                                    top: 1.46 *
                                                        SizeConfig
                                                            .heightMultiplier,
                                                  ),
                                                  child: GestureDetector(
                                                    onTap: () {
                                                      showModalBottomSheet(
                                                          context: context,
                                                          builder: (builder) {
                                                            return MoreOptions();
                                                          });
                                                    },
                                                    child: new Icon(
                                                      Icons.more_vert,
                                                      color: K_primaryWhite,
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                      subtitle: new Padding(
                                        padding: EdgeInsets.only(top: 0),
                                        child: new Text(
                                          "medias[i].artist",
                                          style: new TextStyle(
                                              fontFamily: "IRANYekanMobile",
                                              fontSize: 1.79 *
                                                  SizeConfig.textMultiplier,
                                              color: Color(0xffF6E9E9),
                                              fontWeight: FontWeight.w300),
                                        ),
                                      ),
                                    ),
                                  ),
                                  i < medias.length - 1
                                      ? new Container(
                                          margin: EdgeInsets.only(
                                              right: 70, left: 20),
                                          height: 0.5,
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.9,
                                          color: Color(0xff4B4747),
                                        )
                                      : new SizedBox(
                                          height: 0,
                                        )
                                ],
                              ),
                            ),
                          ),
                      ],
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 48,
                    color: Color(0xff2891E1),
                    child: MaterialButton(
                      hoverColor: Color(0xff2891E1),
                      // focusColor: Color(0xff2891E1),
                      splashColor: Color(0xff2891E1),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'دنبال کردن کانال',
                            style: TextStyle(fontSize: 16, color: Colors.white),
                          ),
                        ],
                      ),
                      color: K_transparentWhite2,
                      onPressed: () {},
                    ),
                  ),
                ]
              ],
            ),
          ),
        );
      },
    );
  }

  FlatButton playButtonFIrst() => FlatButton(
        color: K_accentBlack,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(90.0),
        ),
        onPressed: NewMusicPlayer.audioPlayerButton,
        child: Padding(
          padding: EdgeInsets.only(left: 5),
          child: Container(
            height: 22,
            width: 22,
            child: SvgPicture.asset(
              'assets/images/play.svg',
              semanticsLabel: 'pause',
              color: K_transparentWhite,
            ),
          ),
        ),
      );

  FlatButton playButton() => FlatButton(
        color: K_accentBlack,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(90.0),
        ),
        onPressed: AudioService.play,
        child: Padding(
          padding: EdgeInsets.only(left: 5),
          child: Container(
            height: 22,
            width: 22,
            child: SvgPicture.asset(
              'assets/images/play.svg',
              semanticsLabel: 'pause',
              color: K_transparentWhite,
            ),
          ),
        ),
      );

  FlatButton pauseButton() => FlatButton(
        color: K_accentBlack,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(90.0),
        ),
        onPressed: AudioService.pause,
        child: Padding(
          padding: EdgeInsets.only(left: 0),
          child: Container(
            height: 22,
            width: 22,
            child: SvgPicture.asset(
              'assets/images/pause.svg',
              semanticsLabel: 'pause',
              color: K_transparentWhite,
            ),
          ),
        ),
      );
  Stream<ScreenState> get _screenStateStream =>
      Rx.combineLatest3<List<MediaItem>, MediaItem, PlaybackState, ScreenState>(
          AudioService.queueStream,
          AudioService.currentMediaItemStream,
          AudioService.playbackStateStream,
          (queue, mediaItem, playbackState) =>
              ScreenState(queue, mediaItem, playbackState));

  Widget positionIndicator(MediaItem mediaItem, PlaybackState state) {
    double seekPos;
    return StreamBuilder(
      stream: Rx.combineLatest2<double, double, double>(
          _dragPositionSubject.stream,
          Stream.periodic(Duration(milliseconds: 200)),
          (dragPosition, _) => dragPosition),
      builder: (context, snapshot) {
        double position =
            snapshot.data ?? state.currentPosition.inMilliseconds.toDouble();
        double duration = mediaItem?.duration?.inMilliseconds?.toDouble();
        return Column(
          children: [
            if (duration != null)
              Slider(
                min: 0.0,
                max: duration,
                value: seekPos ?? max(0.0, min(position, duration)),
                activeColor: K_primaryOrange,
                inactiveColor: K_accentBlack,
                onChanged: (value) {
                  _dragPositionSubject.add(value);
                },
                onChangeEnd: (value) {
                  AudioService.seekTo(Duration(milliseconds: value.toInt()));
                  // Due to a delay in platform channel communication, there is
                  // a brief moment after releasing the Slider thumb before the
                  // new position is broadcast from the platform side. This
                  // hack is to hold onto seekPos until the next state update
                  // comes through.
                  // TODO: Improve this code.
                  seekPos = value;
                  _dragPositionSubject.add(null);
                },
              ),
            Row(
              children: <Widget>[
                Text(
                  "${state.currentPosition.inMinutes.toString().padLeft(2, '0')}:${state.currentPosition.inSeconds.remainder(60).toString().padLeft(2, '0')}",
                  style: new TextStyle(color: K_primaryWhite),
                )
              ],
            )
          ],
        );
      },
    );
  }
}

class ScreenState {
  final List<MediaItem> queue;
  final MediaItem mediaItem;
  final PlaybackState playbackState;

  ScreenState(this.queue, this.mediaItem, this.playbackState);
}

// NOTE: Your entrypoint MUST be a top-level function.
void _audioPlayerTaskEntrypoint() async {
  await AudioServiceBackground.run(() => AudioPlayerTask());
}
