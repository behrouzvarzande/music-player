import 'package:avapardaz_music/NetworkUtils/Network_Utils.dart';
import 'package:avapardaz_music/Theme/Consts.dart';
import 'package:avapardaz_music/Theme/sizeConfig.dart';
import 'package:avapardaz_music/Ui/Components/TextFieldComponen.dart';
import 'package:avapardaz_music/Ui/Components/LoginButton.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final GlobalKey<FormBuilderState> formKey = GlobalKey<FormBuilderState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Directionality(
        textDirection: TextDirection.rtl,
        child: SafeArea(
            child: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height + 28,
            color: K_primaryBlack,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(top: 20, right: 20),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Align(
                            alignment: Alignment.centerRight,
                            child: SvgPicture.asset(
                              'assets/images/close.svg',
                              height: 15,
                              width: 15,
                              color: K_accentblackLighter,
                            )),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 30),
                      child: new Text(
                        'ثبت نام در جانا',
                        style: TextStyle(
                            color: K_LightPurpule,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
                FormBuilder(
                  key: formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      LoginButton(
                          onPressed: () {},
                          title: "ورود با حساب گوگل",
                          width: double.infinity,
                          height: 50,
                          borderRadius: 10,
                          gradient: false,
                          backGroundColor: Colors.white,
                          textStyle:
                              TextStyle(color: Colors.black, fontSize: 14),
                          icon: Image(
                            image: AssetImage('assets/images/google.png'),
                            height: 30,
                            width: 30,
                          ),
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          textDirection: TextDirection.rtl,
                          active: true),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 20),
                        child: Row(children: <Widget>[
                          Expanded(
                            child: new Container(
                                margin: const EdgeInsets.only(
                                    left: 10.0, right: 30.0),
                                child: Divider(
                                  color: K_primaryWhite,
                                  height: 4.61 * SizeConfig.heightMultiplier,
                                )),
                          ),
                          Text(
                            "یا",
                            style: TextStyle(
                                color: K_primaryWhite,
                                fontSize: 1.7 * SizeConfig.textMultiplier),
                          ),
                          Expanded(
                            child: new Container(
                                margin: const EdgeInsets.only(
                                    left: 30.0, right: 10.0),
                                child: Divider(
                                  color: K_primaryWhite,
                                  height: 4.61 * SizeConfig.heightMultiplier,
                                )),
                          ),
                        ]),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: TextFieldComponent(
                          attribute: 'name',
                          hintText: '',
                          labelText: 'نام کامل',
                          requiredField: false,
                          needChecking: false,
                        ),
                      ),
                      Padding(
                        padding:
                            const EdgeInsets.only(right: 20, left: 20, top: 20),
                        child: TextFieldComponent(
                          attribute: 'email',
                          hintText: '',
                          labelText: 'ایمیل',
                          requiredField: false,
                          needChecking: false,
                          validators: [
                            FormBuilderValidators.email(
                                errorText: 'ایمیل نامعتبر')
                          ],
                        ),
                      ),
                      Padding(
                        padding:
                            const EdgeInsets.only(right: 20, left: 20, top: 20),
                        child: TextFieldComponent(
                          attribute: 'phone',
                          hintText: '',
                          labelText: 'موبایل (اختیاری)',
                          requiredField: false,
                          needChecking: false,
                          validators: [
                            FormBuilderValidators.pattern(
                              '^(\\+98|0098|98|0)?9\\d{9}\$',
                              errorText: "شماره نامعتبر",
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            right: 20, left: 20, top: 20, bottom: 20),
                        child: TextFieldComponent(
                          attribute: 'password',
                          hintText: '',
                          labelText: 'کلمه عبور',
                          requiredField: false,
                          needChecking: false,
                          validators: [
                            // FormBuilderValidators.minLength(5,errorText: 'حداقل ۵ کاراکتر')
                          ],
                        ),
                      ),
                      LoginButton(
                          onPressed: () async{
                             if (formKey.currentState.saveAndValidate()) {
                              print(formKey.currentState.value);
                             if(await NetworkUtil.internal().postSignUp(formKey.currentState.value)){
                                Navigator.pushReplacementNamed(context, "/root");
                             } 
                            }
                          },
                          title: "ورود",
                          gradient: true,
                          width: double.infinity,
                          height: 50,
                          borderRadius: 10,
                          backGroundColor: K_LightPurpule,
                          textStyle:
                              TextStyle(color: Colors.white, fontSize: 14),
                          icon: null,
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          textDirection: TextDirection.rtl,
                          active: true),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'حساب کاربری دارید؟',
                        style: TextStyle(
                          color: K_accentBlack,
                        ),
                      ),
                      Text(
                        '  وارد شوید',
                        style: TextStyle(
                            color: K_accentblackLighter, fontSize: 12),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        )),
      ),
    );
  }
}
