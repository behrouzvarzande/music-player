import 'package:audio_service/audio_service.dart';
import 'package:avapardaz_music/Blocs/PlayBloc.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BottomPlayer extends StatelessWidget {
  BottomPlayer({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Selector<PlayBloc, List<MediaItem>>(
      selector: (_, bloc) => bloc.medias,
      builder: (context, medias, child) {
        if ((context
                    .select<PlayBloc, List<MediaItem>>((bloc) => bloc?.medias)
                    ?.length ??
                0) >
            0) {
          return Container(color: Color(0xff363333), child: child);
        }
        return Container();
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Selector<PlayBloc, MediaItem>(
              selector: (_, bloc) => bloc.currentMediaItem,
              builder: (context, currentMediaItem, child) => Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  ///image
                  Container(
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: NetworkImage(currentMediaItem.artUri),
                          fit: BoxFit.fill),
                      shape: BoxShape.circle,
                    ),
                  ),

                  ///Texts
                  Padding(
                    padding: const EdgeInsets.only(left: 8),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Container(
                          width: 150,
                          child: Text(
                            currentMediaItem.title,
                            //"A Salute To Head-Scratching Science",
                            style: TextStyle(
                                fontSize: 16, color: Color(0xffF6E9E9)),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        Container(
                          width: 150,
                          child: Text(
                            currentMediaItem.artist,
                            //"Science Friday and WNYC Studios",
                            style: TextStyle(
                                fontSize: 12, color: Color(0xffF6E9E9)),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          Selector<PlayBloc, bool>(
            selector: (_, bloc) => bloc.isPlaying,
            builder: (context, isPlaying, child) => Row(
              children: <Widget>[
                ///play Icon
                if (isPlaying)
                  IconButton(
                      icon: Icon(Icons.pause, color: Colors.white),
                      onPressed: () =>
                          Provider.of<PlayBloc>(context, listen: false).pause())
                else
                  IconButton(
                      icon: Icon(Icons.play_arrow, color: Colors.white),
                      onPressed: () =>
                          Provider.of<PlayBloc>(context, listen: false).play()),

                ///Next icon
                IconButton(
                    icon: Icon(Icons.skip_next, color: Colors.white),
                    onPressed: () =>
                        Provider.of<PlayBloc>(context, listen: false).next())
              ],
            ),
          )
        ],
      ),
    );
  }
}
