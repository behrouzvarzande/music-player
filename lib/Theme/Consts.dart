import 'package:flutter/cupertino.dart';

const Color K_primaryOrange = Color(0xffE31140);
const Color K_accentblackLighter = Color(0xff8B8B8B);
const Color K_primaryBlack = Color(0xff272121);
const Color K_accentBlack = Color(0xff363333);
const Color K_transparentWhite = Color(0xf0ffffff);
const Color K_transparentWhite2 = Color(0x2affffff);
const Color K_primaryWhite = Color(0xffF6E9E9);
const Color K_LightPurpule = Color(0xffFF2153);
const Color K_DarkPurple = Color(0xffE31140);
