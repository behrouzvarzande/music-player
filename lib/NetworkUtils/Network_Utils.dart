import 'dart:convert';

import 'package:audio_service/audio_service.dart';
import 'package:avapardaz_music/Api/Api.dart';
import 'package:avapardaz_music/Models/Channel.dart';
import 'package:avapardaz_music/Models/Artist.dart';
import 'package:avapardaz_music/Models/Geners.dart';
import 'package:avapardaz_music/Models/PlayList.dart';
import 'package:avapardaz_music/Models/Singer.dart';
import 'package:avapardaz_music/Models/Song.dart';
import 'package:avapardaz_music/Models/User.dart';
import 'package:avapardaz_music/main.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

import '../main.dart';

class NetworkUtil {
  static NetworkUtil _instance = new NetworkUtil.internal();

  NetworkUtil.internal();

  factory NetworkUtil() => _instance;

  Future<bool> getChannels() async {
    Client client = Client();
    Uri uri = Uri.https(Api.baseUrl, Api.channels);
    Response response = await client.get(uri);

    List<Channel> channelModel = new List<Channel>();
    for (var item in json.decode(response.body)) {
      channelModel.add(Channel(
        name: item['name'],
        coverImage: item['coverImage'],
        bgColor: item['bgColor'] != null
            ? Color(int.parse(item['bgColor'].replaceAll("#", "ff"), radix: 16))
            : Colors.white,
        id: item['id'],
        //TODO: must implement others after server side completed
      ));
    }

    channelsData = channelModel;
    if (response.statusCode == 200) {
      print('true');
      return true;
    } else {
      print('false');
      return false;
    }
  }

  Future<bool> getSongs() async {
    Client client = Client();
    Uri uri = Uri.https(Api.baseUrl, Api.musics);
    Response response = await client.get(uri);

    // print(json.decode(response.body));
    List<Song> songsModel = new List<Song>();
    for (var item in json.decode(response.body)) {
      songsModel.add(Song(
        id: item['_id'],
        singers: [Singer(name: "فرزاد فرخئ")],
        name: item['name'],
        coverArt: item['coverImage'],
        releaseDate: item['releaseDate'],
        audioLink: item['audioLink'],
        //geners: (item['genres'] as List).map((e) => null),
        // singers: (item['singer'] as List)
        //     .map((e) => new Singer(
        //           name: e['name'],
        //           id: e['_id'],
        //         ))
        //     .toList(),
      ));
    }
    songsData = songsModel;
    for (var item in songsData) {
      MediaItem a = MediaItem(
        id: item.audioLink,
        album: item.singers[0].name,
        title: item.name,
        artUri: item.coverArt,
        artist: "item.singers[0].name",
        duration: Duration(milliseconds: 240000),
      );
      queueBackGround.add(a);
    }
    print(
        "********************************************************************");
    print(queueBackGround.length);
    //AudioServiceBackground.setQueue(queueBackGround);
    //print(queueBackGround[0].artUri);
    print(
        "********************************************************************");

    if (response.statusCode == 200) {
      print('true');
      return true;
    } else {
      print('false');
      return false;
    }
  }

  Future<bool> getArtist() async {
    Client client = Client();
    Uri uri = Uri.https(Api.baseUrl, Api.artists);
    Response response = await client.get(uri);
    List<dynamic> data = json.decode(response.body);
    List<Artist> artistModel = data.map((e) => Artist.fromJson(e)).toList();
    artistData = artistModel;
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> getChannelsMusic(String channelID) async {
    Client client = Client();

    Uri uri = Uri.https(
      Api.baseUrl,
      Api.channelMusics(channelID),
    ); //TODO
    Response respons = await client.get(uri);
    List<Song> channelsSong = new List<Song>();
    var item = json.decode(respons.body);
    channelsSong = (item['musics'] as List)
        .map((e) => new Song(
              id: e['_id'],
              singers: [Singer(name: "فرزاد فرخئ")],
              name: e['name'],
              coverArt: e['coverImage'],
              releaseDate: e['releaseDate'],
              audioLink: e['audioLink'],
            ))
        .toList();
    channelClickedSongs.clear();
    for (var item in channelsSong) {
      channelClickedSongs.add(MediaItem(
        id: item.audioLink,
        album: item.singers[0].name,
        title: item.name,
        artUri: item.coverArt,
        artist: "item.singers[0].name",
        duration: Duration(milliseconds: 240000),
      ));
    }
    // print(channelClickedSongs.length);
    if (respons.statusCode == 200) {
      //  print('true');
      return true;
    } else {
      print('false');
      return false;
    }
  }

  Future<bool> getGenres() async {
    Client client = Client();
    Uri uri = Uri.https(Api.baseUrl, Api.geners);
    Response response = await client.get(uri);

    // print(json.decode(response.body));
    List<Gener> genersModel = new List<Gener>();
    for (var item in json.decode(response.body)) {
      genersModel.add(Gener(
        name: item['name'],
        coverArt: item['coverImage'],
        filterColor: Color(
            int.parse(item['filterColor'].replaceAll("#", "ff"), radix: 16)),
        id: item['_id'],
        //TODO : Get Others From Server Later
      ));
    }
    genersData = genersModel;
    if (response.statusCode == 201) {
      print('true');
      return true;
    } else {
      print('false');
      return false;
    }
  }

  Future<bool> postSignUp(Map<String, dynamic> body) async {
    Client client = Client();
    Map<String, String> headers = {
      "Content-Type": "application/json",
    };
    var body2 = utf8.encode(json.encode(body));
    Uri uri = Uri.https(Api.baseUrl, Api.signUp);
    Response respons = await client.post(
      uri,
      body: body2,
      headers: headers,
    );

    var item = json.decode(respons.body);
    User.accessToken = item['authToken'];
    Map<String, dynamic> customer = item['customer'];
    User.name = customer['name'];
    User.email = customer['email'];
    User.phone = customer['phone'];

    if (respons.statusCode == 201) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> postLogin(Map<String, dynamic> body) async {
    Client client = Client();
    Map<String, String> headers = {
      "Content-Type": "application/json",
    };
    var body2 = utf8.encode(json.encode(body));
    Uri uri = Uri.https(Api.baseUrl, Api.login);
    Response respons = await client.post(
      uri,
      body: body2,
      headers: headers,
    );

    var item = json.decode(respons.body);
    User.accessToken = item['token'];
    if (respons.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> getUserInfo(String token) async {
    Client client = Client();

    Map<String, String> headers = {
      "x-auth-token": token,
    };

    Uri uri = Uri.https(Api.baseUrl, Api.userInfo);
    Response respons = await client.get(
      uri,
      headers: headers,
    );

    var item = json.decode(respons.body);

    User.name = item['name'];
    User.email = item['email'];
    User.phone = item['phone'];

    print('-----------------------------------------');
    print(User.email);

    if (respons.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }
}

class Customer {
  String name;
  String email;
  String phone;
  List<PlayList> playLis;
  List<Channel> followedChannels;
  Customer(
      {this.name, this.email, this.followedChannels, this.phone, this.playLis});
}
