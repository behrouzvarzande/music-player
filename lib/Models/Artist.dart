import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';


part 'Artist.g.dart'; 

@JsonSerializable(explicitToJson: true)
class Artist{
 final String id;
 final String name;
final  String coverArt;
  int songsCount;
final  String bio;

  Artist({@required this.id,@required this.name,@required this.coverArt,@required this.bio});

  factory Artist.fromJson(Map<String,dynamic> data) => _$ArtistFromJson(data);

  Map<String,dynamic> toJson() => _$ArtistToJson(this);
  
}