import 'package:avapardaz_music/Theme/Consts.dart';
import 'package:avapardaz_music/Ui/Components/TextFieldComponen.dart';
import 'package:avapardaz_music/Ui/Components/LoginButton.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ForgotPassConfirmPage extends StatefulWidget {
  @override
  _ForgotPassConfirmPageState createState() => _ForgotPassConfirmPageState();
}

class _ForgotPassConfirmPageState extends State<ForgotPassConfirmPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Directionality(
        textDirection: TextDirection.rtl,
        child: SafeArea(
            child: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height - 27,
            color: K_primaryBlack,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(top: 20, right: 20),
                      child: Align(
                          alignment: Alignment.centerRight,
                          child: SvgPicture.asset(
                            'assets/images/close.svg',
                            height: 15,
                            width: 15,
                            color: K_accentblackLighter,
                          )),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 30),
                      child: new Text(
                        'فراموشی کلمه عبور',
                        style: TextStyle(
                            color: K_LightPurpule,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                          right: 20, left: 20, bottom: 20),
                      child: TextFieldComponent(
                        attribute: 'email or phone',
                        hintText: '- - - -',
                        showCursor: false,
                        labelText: 'کد تایید را وارد کنید',
                        requiredField: false,
                        needChecking: false,
                      ),
                    ),
                    LoginButton(
                        title: "ادامه",
                        gradient: true,
                        width: double.infinity,
                        height: 50,
                        borderRadius: 10,
                        backGroundColor: K_LightPurpule,
                        textStyle: TextStyle(color: Colors.white, fontSize: 14),
                        icon: null,
                        padding:
                            EdgeInsets.only(right: 20, left: 20, bottom: 0),
                        textDirection: TextDirection.rtl,
                        active: true),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: GestureDetector(
                    onTap: () {},
                    child: Text(
                      'بازگشت',
                      style: TextStyle(
                        color: K_accentblackLighter,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        )),
      ),
    );
  }
}
