import "package:avapardaz_music/Models/Singer.dart";
import "package:avapardaz_music/Models/Geners.dart";
import 'package:flutter/material.dart';

class Song {
  final String id;
  final String name;
  final String releaseDate;
  final String coverArt;
  final List<Singer> singers;
  int likes;
  int views;

  final String audioLink;
  String lyric;
  Duration length;

  List<Gener> geners;
  Song({
    @required this.id,
    @required this.name,
    @required this.singers,
    @required this.coverArt,
    @required this.audioLink,
    // @required this.length,
    //@required this.lyric,
    @required this.releaseDate,
    // @required this.geners
  });
  String get songName => name;
  //Singer get songSinger => singers;
  String get songImage => coverArt;
  String get songLink128 => audioLink;
  String get songLyrics => lyric;
  Duration get songLength => length;
  String get songReleaseDate => releaseDate;
  List<Gener> get songGeners => geners;

  // factory Song.fromJson(String str) => Song.fromMap(json.decode(str));

  // String toJson() => json.encode(toMap());

  // factory Song.fromMap(Map<String, dynamic> json) => Song(
  //     name: json["name"] == null ? "" : json["name"],
  //     coverArt: json["coverArt"] == null ? "" : json["coverArt"],
  //     releaseDate: json["releaseDate"] == null ? "" : json["releaseDate"],
  //     link128: json["link128"] == null ? "" : json["link128"],
  //     url320: json["link320"] == null ? "" : json["link320"],
  //     singer: json["singer"] == null
  //         ? new Singer(name: "", coverArt: "")
  //         : Singer.fromJson(json["singer"]),
  //     geners: json["genre"] == null
  //         ? null
  //         : List<Gener>.from(json["genre"].map((x) => Gener.fromJson(x))));

  // Map<String, dynamic> toMap() => {
  //       "name": name,
  //       "coverArt": coverArt,
  //       "releaseDate": releaseDate,
  //       "link128": link128,
  //       "link320": url320,
  //       "singer": singer == null ? null : singer.toJson(),
  //       "genre": geners == null
  //           ? null
  //           : List<dynamic>.from(geners.map((e) => e.toJson()))
  //     };
}
