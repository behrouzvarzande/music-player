enum ResponseStatus { Unauthorized, Success, Fail }

class Api {
  static String apiKey = '';
  static String baseUrl = "backend-janaaa.liara.run";

  static String auth = "/api/user/authenticate";
  static String register = "/api/usear/register";

  static String channels = "/api/channels";

  static String channelMusics(String id) => "/api/channels/$id";

  static String geners = "/api/genres";

  static String generMusics(String id) => "/api/genre/musics?_id=$id";

  static String artists = "/api/artists";

  static String artistMusics(String id) => "/api/artist/musics?_id=$id";

  static String albums = "/api/albums";

  static String albumMusics(String id) => "/api/album/musics?_id=$id";

  static String musics = "/api/musics";

  static String signUp ="/api/customers";

  static String login ="/api/auth";

  static String userInfo ="/api/customers/me";
}
