import 'package:audio_service/audio_service.dart';
import 'package:avapardaz_music/Ui/Pages/NewMusicPlayer/AudioPlayerTask.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../main.dart';

/// this is bloc used for player
/// based on provider 4.3.2
/// TODO tasks
/// 1. return the list page by page
///
class PlayBloc with ChangeNotifier, DiagnosticableTreeMixin {
  List<MediaItem> get medias => AudioService?.queue ?? new List<MediaItem>();
  MediaItem get currentMediaItem => AudioService.currentMediaItem;
  bool get isPlaying => AudioService?.playbackState?.playing ?? false;
  int get playingChannelIndex => _playingChannelIndex;
  int _playingChannelIndex = -1;
  set channelIndex(int index) {
    _playingChannelIndex = index;
  }

  void _audioPlayerTaskEntrypoint() async {
    AudioServiceBackground.run(() => AudioPlayerTask());
  }

  void addMediaItem(MediaItem media) {
    AudioService.addQueueItem(media);
    notifyListeners();
  }

  void removeMedia(MediaItem media) {
    AudioService.removeQueueItem(media);
    notifyListeners();
  }

  void clearQueue() {
    AudioService.queue.clear();
    notifyListeners();
  }

  void addMediaItems(List<MediaItem> items, bool clearBeforeAdd) {
    if (clearBeforeAdd) {
      queueBackGround.clear();
      queueBackGround = items;
    } else {
      AudioService.addQueueItems(items);
    }
    notifyListeners();
  }

  void seekTo(Duration finalPositon) {
    AudioService.seekTo(finalPositon);
    notifyListeners();
  }

  void next() {
    AudioService.skipToNext();
    notifyListeners();
  }

  void previous() {
    AudioService.skipToPrevious();
    notifyListeners();
  }

  void stop() {
    AudioService.queue.clear();
    AudioService.stop();
    notifyListeners();
  }

  start() async {
    List<dynamic> list = List();
    for (int i = 0; i < queueBackGround.length; i++) {
      var m = queueBackGround[i].toJson();
      list.add(m);
    }
    var params = {'data': list};
    // var a = queueBackGround.length;
    await AudioService.start(
      backgroundTaskEntrypoint: _audioPlayerTaskEntrypoint,
      androidNotificationChannelName: 'Audio Player',
      androidNotificationColor: 0xFF2196f3,
      androidNotificationIcon: 'mipmap/ic_launcher',
      params: params,
    );
  }

  Future<void> play() async {
    try {
      List<dynamic> list = List();
      for (int i = 0; i < queueBackGround.length; i++) {
        var m = queueBackGround[i].toJson();
        list.add(m);
      }

      var params = {"data": list};

      AudioService.start(
        backgroundTaskEntrypoint: _audioPlayerTaskEntrypoint,
        androidNotificationChannelName: 'Audio Service Demo',
        // Enable this if you want the Android service to exit the foreground state on pause.
        //androidStopForegroundOnPause: true,
        params: params,
        androidNotificationColor: 0xFF2196f3,
        androidNotificationIcon: 'mipmap/ic_launcher',
        androidEnableQueue: true,
      );
    } catch (e) {
      print('play exception');
    }
    AudioService.play();
    notifyListeners();
  }

  Future<void> playFromList(List<MediaItem> list) async {
    AudioService.queue.clear();
    if ((AudioService.queue?.length ?? 0) == 0) {
      try {
        List<dynamic> _list = List();
        for (int i = 0; i < list.length; i++) {
          var m = list[i].toJson();
          _list.add(m);
        }
        var params = {"data": _list};
        AudioService.start(
          backgroundTaskEntrypoint: _audioPlayerTaskEntrypoint,
          androidNotificationChannelName: 'Audio Service Demo',
          // Enable this if you want the Android service to exit the foreground state on pause.
          //androidStopForegroundOnPause: true,
          params: params,
          androidNotificationColor: 0xFF2196f3,
          androidNotificationIcon: 'mipmap/ic_launcher',
          androidEnableQueue: true,
        );
      } catch (e) {}
    }
    AudioService.play();
    notifyListeners();
  }

  void pause() {
    AudioService.pause();
    notifyListeners();
  }

  void skipToQueuItem(String offset) {
    AudioService.skipToQueueItem(offset);
    notifyListeners();
  }

  void skipAndAddItem(MediaItem item) {
    AudioService.addQueueItem(item);
    AudioService.skipToQueueItem(item.id);
    notifyListeners();
  }
}
