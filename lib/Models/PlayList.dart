class PlayList{
  final String name;
  final String topimageURL;
  final String secondimageURL;
  final String thirdimageURL;

  PlayList(this.name, this.topimageURL, this.secondimageURL, this.thirdimageURL);
}
List<PlayList> playListData=[
  new PlayList(
      "نوستالوژی",
      "https://mirror.emsn.ir/music/kochebazar.jpg",
      "https://mirror.emsn.ir/music/taxi.jpg",
      "https://mirror.emsn.ir/music/morning.jpg"
  ),
  new PlayList(
      "درام",
     "https://mirror.emsn.ir/music/morning.jpg",
      "https://mirror.emsn.ir/music/taxi.jpg",
      "https://mirror.emsn.ir/music/morning.jpg"
  ),
  new PlayList(
      "رپ",
      "https://mirror.emsn.ir/music/manfi.jpg",
      "https://mirror.emsn.ir/music/taxi.jpg",
      "https://mirror.emsn.ir/music/morning.jpg"
  ),
  new PlayList(
      "شاد",
      "https://mirror.emsn.ir/music/taxi.jpg",
      "https://mirror.emsn.ir/music/happy.jpg",
      "https://mirror.emsn.ir/music/morning.jpg"
  ),
];