import 'package:audio_service/audio_service.dart';
import 'package:avapardaz_music/Navigator/NestedNavigator.dart';
import 'package:avapardaz_music/Ui/Pages/AboutSinger/AboutSinger.dart';
import 'package:avapardaz_music/Ui/Pages/Explore/ExplorePage.dart';
import 'package:avapardaz_music/Ui/Pages/HomePage/HomePage.dart';
import 'package:avapardaz_music/Ui/Pages/Library/LibraryPage.dart';
import 'package:avapardaz_music/Ui/Pages/NewMusicPlayer/MusicPlayerNew.dart';
import 'package:avapardaz_music/Ui/Pages/Profile/ProfilesPage.dart';
import 'package:avapardaz_music/Ui/Pages/Root/RootPage.dart';
import 'package:flutter/material.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'BottomPlayer.dart';

class SlidingUpPanelComponent extends StatefulWidget {
  static PanelController panelController = new PanelController();

  static void open() => panelController.open();

  static void close() => panelController.close();
  @override
  _SlidingUpPanelComponentState createState() =>
      _SlidingUpPanelComponentState();
}

class _SlidingUpPanelComponentState extends State<SlidingUpPanelComponent> {
  double _panelHeightOpen;
  static List<Widget> _widgetOptions = <Widget>[
    HomePage(),
    ExplorePage(),
    LibraryPage(),
    ProfilePage(),
  ];
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  final double _initFabHeight = 120.0;

  double _panelHeightClosed;
  double _fabHeight;

  double _bodyBottomMargin = 128.0;

  @override
  void initState() {
    super.initState();

    _fabHeight = _initFabHeight;
  }

  @override
  Widget build(BuildContext context) {
    _panelHeightOpen = MediaQuery.of(context).size.height * .89;
    _panelHeightClosed = 75;

    return SlidingUpPanel(
        controller: SlidingUpPanelComponent.panelController,
        maxHeight: _panelHeightOpen,
        minHeight: _panelHeightClosed,
        renderPanelSheet: true,
        parallaxOffset: .5,
        defaultPanelState: PanelState.CLOSED,
        isDraggable: true,
        backdropColor: Colors.red,
        body: GestureDetector(
          onTap: () => null,
          child: Container(
            margin: EdgeInsets.only(bottom: _bodyBottomMargin),
            child: body(),
          ),
        ),
        panelBuilder: (sc) => _panel(sc),
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(18.0), topRight: Radius.circular(18.0)),
        onPanelSlide: (double pos) => setState(() {
              _fabHeight = pos * (_panelHeightOpen - _panelHeightClosed) +
                  _initFabHeight;
            }),
        collapsed: BottomPlayer());
  }

  Widget _panel(ScrollController sc) {
    return AudioServiceWidget(
        child: NewMusicPlayer(
      panelController: SlidingUpPanelComponent.panelController,
      sc: sc,
    ));
  }

  body() {
    return AudioServiceWidget(
      child: NestedNavigator(
        navigationKey: RootPage.navigationKey,
        initialRoute: '/',
        routes: {
          // default rout as '/' is necessary!
          '/': (context) => _widgetOptions[RootPage.selectedIndex],
          '/two': (context) => AboutSinger(),
        },
      ),
    );
  }
}
