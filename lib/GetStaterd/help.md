# Project Standards
`Please Read All of this and do all steps 😉 `
> you can use git command help and markdown help too
## Implemenet Models
1. find Lib/Models
2. create New Class with proper name
3. define fields and constrator using `@required`
4. implement toJson And fromJson methods
5. add Model Documentation to Models.md
6. don't forgot  push to a test branch(all except : master and develop ) and ask for final merge

## Implement Pages
1. find Lib/Ui/Pages
2. create a new branch using git commands(relatead name){ your init commit must equal to develop branch}
3. create a Folder and inner class (statefull/stateless/...) with proper name
4. Write Code Component based please
    ### Implement Component
    1. find Lib/Ui/Components
    2. create a class (statefull/stateless/...) with proper name
    3. please don't (fix elments so much and create similar components) 
5. after finishing code push to git repository
6. please write Docoumentation into ReadMe.md 
7. merge stable branch( develop) into your branch
8. fix conflicts and ask for final test and merge

## Add new Api
1. find Lib/Api/Api.dart
2. Add url to Api class and documentation to Api.md
3. goto Lib/NetworkUtils/NetworkUtils.dart or create new class near it
3. write parameters 
4. send request 
5. use toJson and fromJson methods to get or put your data
6. don't forgot  push to a test branch(all except : master and develop ) and ask for final merge
## Add new theme
1. find Lib/Theme
2. create your class and theme
3. write documentation on theme.md
3. don't forgot  push to a test branch(all except : master and develop ) and ask for final merge




