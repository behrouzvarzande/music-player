import 'package:audio_service/audio_service.dart';
import 'package:avapardaz_music/Blocs/PlayBloc.dart';
import 'package:avapardaz_music/Models/Channel.dart';
import 'package:avapardaz_music/Models/Geners.dart';
import 'package:avapardaz_music/Models/Song.dart';
import 'package:avapardaz_music/Models/Artist.dart';
import 'package:avapardaz_music/Ui/Pages/ForgotPassword/ForgotPassConfirm.dart';
import 'package:avapardaz_music/Ui/Pages/ForgotPassword/ForgotPassRequestPage.dart';
import 'package:avapardaz_music/Ui/Pages/Login/LoginPage.dart';
import 'package:avapardaz_music/Ui/Pages/SignUp/SignUpPage.dart';
import 'package:avapardaz_music/Ui/Pages/SplashScreen/SplashPage.dart';
import 'package:avapardaz_music/Ui/Pages/Root/RootPage.dart';
import 'package:avapardaz_music/Theme/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MultiProvider(
    child: MyApp(),
    providers: [
      ChangeNotifierProvider(
        create: (context) => PlayBloc(),
      )
    ],
  ));

//      MyApp());
}
//*************************************************************************** */
// ! Models Are here

List<Channel> channelsData = List<Channel>();
List<Song> songsData = List<Song>();
List<Artist> artistData = new List<Artist>();
List<Gener> genersData = new List<Gener>();
List<MediaItem> queueBackGround = [];
int songsPlayIndex = 0;
List<MediaItem> channelClickedSongs = new List<MediaItem>();
bool clicked = false;
int clickedIndex;

// ! Done!
//************************************************************************* */
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return OrientationBuilder(
          builder: (context, orientation) {
            SizeConfig().init(constraints, orientation);
            return MaterialApp(
              title: "Jana",
              debugShowCheckedModeBanner: false,
              initialRoute: '/',
              theme: ThemeData(
                fontFamily: 'IRANYekanMobile',
                canvasColor: Colors.transparent,
              ),
              routes: {
                // When navigating to the "/" route, build the FirstScreen widget.
                '/': (context) => SplashPage(),
                // When navigating to the "/second" route, build the SecondScreen widget.
                '/root': (context) => RootPage(),
                '/login': (context) => LoginPage(),
                '/signUp': (context) => SignUpPage(),
                '/forgot1': (context) => ForgotPassRequestPage(),
                '/forgot2': (context) => ForgotPassConfirmPage(),
              },
            );
          },
        );
      },
    );
  }
}
