import 'package:avapardaz_music/Ui/Components/LibraryArtistModel.dart';
import 'package:avapardaz_music/Ui/Components/LibraryChannelsModel.dart';
import 'package:avapardaz_music/Models/PlayList.dart';
import 'package:avapardaz_music/NetworkUtils/Network_Utils.dart';
import 'package:avapardaz_music/Ui/Components/LibrarySongModel.dart';
import 'package:avapardaz_music/Ui/Components/Loading.dart';
import 'package:avapardaz_music/main.dart';
import 'package:avapardaz_music/Theme/sizeConfig.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../../Theme/Consts.dart';

class LibraryPage extends StatefulWidget {
  @override
  _LibraryPageState createState() => _LibraryPageState();
}

class _LibraryPageState extends State<LibraryPage> with TickerProviderStateMixin,AutomaticKeepAliveClientMixin<LibraryPage> {
  TabController tabControllerMain;
  TabController tabControllerSecond;
  Map<String, SliverAppBar> appBarList;
  BuildContext _context;
  int _currenttab = 0;
  double _dividerWidth = 0;
  @override
  void initState() {
    super.initState();
    tabControllerMain =
        new TabController(initialIndex: 0, length: 4, vsync: this);
    tabControllerSecond =
        new TabController(initialIndex: 0, length: 4, vsync: this);
    _dividerWidth = 80;
    Future.delayed(Duration.zero, () {
      _dividerWidth = _dividerWidth = (MediaQuery.of(context).size.width) / 5;
    });
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    tabControllerSecond.addListener(() {
      switch (tabControllerSecond.index) {
        case 0:
          _currenttab = 0;
          setState(() {
            _dividerWidth =
                _dividerWidth = (MediaQuery.of(context).size.width) / 5;
            NetworkUtil.internal().getSongs();
          });

          break;
        case 1:
          _currenttab = 1;
          setState(() {
            _dividerWidth =
                _dividerWidth = ((MediaQuery.of(context).size.width) / 5) * 2;
          });

          break;
        case 2:
          _currenttab = 2;
          setState(() {
            _dividerWidth =
                _dividerWidth = ((MediaQuery.of(context).size.width) / 5) * 3;
            NetworkUtil.internal().getChannels();
          });
          break;
        case 3:
          _currenttab = 3;
          setState(() {
            _dividerWidth = _dividerWidth =
                (MediaQuery.of(context).size.width) -
                    12.16 * SizeConfig.widthMultiplier;
          });
          break;
      }
    });
    return new Scaffold(
      backgroundColor: K_primaryBlack,
      body: Directionality(
        textDirection: TextDirection.rtl,
        child: SafeArea(
          child: Directionality(
              textDirection: TextDirection.rtl,
              child: Container(
                child: new Column(
                  children: [
                    new Row(
                      children: [
                        new TabBar(
                          indicatorColor: Color(0x00ffffff),
                          labelColor: K_primaryWhite,
                          controller: tabControllerMain,
                          isScrollable: true,
                          labelStyle: new TextStyle(
                              fontFamily: "IRANYekanMobile",
                              fontSize: 3.5 * SizeConfig.textMultiplier,
                              fontWeight: FontWeight.bold),
                          onTap: (index) {
                            switch (index) {
                              case 0:
                                _currenttab = 0;
                                break;
                              case 1:
                                _currenttab = 1;
                                break;
                              case 2:
                                _currenttab = 2;
                                break;
                              case 3:
                                _currenttab = 3;
                                break;
                            }
                          },
                          tabs: <Widget>[
                            Tab(
                              child: new Container(
                                child: new Text('گنجه'),
                              ),
                            ),
                            Tab(
                              child: new Container(
                                child: new Text('2'),
                              ),
                            ),
                            Tab(
                              child: new Container(
                                child: new Text('3'),
                              ),
                            ),
                            Tab(
                              child: new Container(
                                child: new Text('4'),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                    Expanded(
                      child: Container(
//                      height: MediaQuery.of(context).size.height -
//                          18.74 * SizeConfig.heightMultiplier,
                        child: TabBarView(
                            controller: tabControllerMain,
                            children: <Widget>[
                              new Column(
                                children: [
                                  new Row(
                                    children: [
                                      new TabBar(
                                        indicatorColor: Color(0x00ffffff),
                                        labelColor: K_primaryOrange,
                                        controller: tabControllerSecond,
                                        isScrollable: true,
                                        onTap: (index) {
                                          switch (index) {
                                            case 0:
                                              _currenttab = 0;
                                              setState(() {
                                                _dividerWidth = _dividerWidth =
                                                    (MediaQuery.of(context)
                                                            .size
                                                            .width) /
                                                        5;
                                                NetworkUtil.internal()
                                                    .getSongs();
                                              });

                                              break;
                                            case 1:
                                              _currenttab = 1;
                                              setState(() {
                                                _dividerWidth = _dividerWidth =
                                                    ((MediaQuery.of(context)
                                                                .size
                                                                .width) /
                                                            5) *
                                                        2;
                                              });

                                              break;
                                            case 2:
                                              _currenttab = 2;
                                              setState(() {
                                                _dividerWidth = _dividerWidth =
                                                    ((MediaQuery.of(context)
                                                                .size
                                                                .width) /
                                                            5) *
                                                        3;
                                                NetworkUtil.internal()
                                                    .getChannels();
                                              });
                                              break;
                                            case 3:
                                              _currenttab = 3;
                                              setState(() {
                                                _dividerWidth = _dividerWidth =
                                                    (MediaQuery.of(context)
                                                            .size
                                                            .width) -
                                                        12.16 *
                                                            SizeConfig
                                                                .widthMultiplier;
                                              });
                                              break;
                                          }
                                          print(_dividerWidth);
                                        },
                                        unselectedLabelColor: K_primaryWhite,
                                        unselectedLabelStyle: new TextStyle(
                                            fontFamily: "IRANYekanMobile",
                                            fontSize: 1.75 *
                                                SizeConfig.textMultiplier,
                                            fontWeight: FontWeight.w400),
                                        labelStyle: new TextStyle(
                                            fontFamily: "IRANYekanMobile",
                                            fontSize:
                                                2.2 * SizeConfig.textMultiplier,
                                            fontWeight: FontWeight.bold),
                                        tabs: <Widget>[
                                          Tab(
                                            child: new Container(
                                              child: new Text('آهنگ ها'),
                                            ),
                                          ),
                                          Tab(child: new Text("هنرمندان")),
                                          Tab(
                                            child: new Container(
                                              child: new Text('کانال ها'),
                                            ),
                                          ),
                                          Tab(
                                            child: new Container(
                                              child: new Text('پلی لیست ها'),
                                            ),
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                  new Row(
                                    children: [
                                      new Container(
                                        width: _dividerWidth,
                                        height: 1,
                                        color: Colors.red,
                                      ),
                                    ],
                                  ),
                                  Expanded(
                                    child: new Container(
                                      color: K_primaryBlack,
//                                height: MediaQuery.of(context).size.height -
//                                    26.20 * SizeConfig.heightMultiplier,
                                      child: TabBarView(
                                        controller: tabControllerSecond,
                                        children: <Widget>[
                                          ListView.builder(
                                              itemCount: songsData.length,
                                              itemBuilder: (context, index) {
                                                return LibrarySongsModel(
                                                    circleAvatar:
                                                        songsData[index]
                                                            .coverArt,
                                                    title:
                                                        songsData[index].name,
                                                    time: "0",
                                                    subTitle: songsData[index]
                                                        .singers[0]
                                                        .name);
                                              }),
                                          ListView.builder(
                                              itemCount: artistData.length,
                                              itemBuilder: (context, index) {
                                                return Column(
                                                  children: [
                                                    SizedBox(
                                                      height: 1.46 *
                                                          SizeConfig
                                                              .heightMultiplier,
                                                    ),
                                                    LibraryArtistModel(
                                                      circleAvatar:
                                                          artistData[index]
                                                              .coverArt,
                                                      leading: artistData[index]
                                                          .name,
                                                      albumsNo: "5",
                                                      tracksNo:
                                                          artistData[index]
                                                              .songsCount
                                                              .toString(),
                                                    ),
                                                    SizedBox(
                                                      height: 5,
                                                    )
                                                  ],
                                                );
                                              }),
                                          ListView.builder(
                                              itemCount: channelsData.length,
                                              itemBuilder: (context, index) {
                                                return Column(
                                                  children: [
                                                    SizedBox(
                                                      height: 1.46 *
                                                          SizeConfig
                                                              .heightMultiplier,
                                                    ),
                                                    LibraryChannelsModel(
                                                      circleAvatar:
                                                          channelsData[index]
                                                              .coverImage,
                                                      leading:
                                                          channelsData[index]
                                                              .name,
                                                      time: "00:00",
                                                      tracksNo: "0",
                                                      containerColor:
                                                          channelsData[index]
                                                              .bgColor,
                                                    ),
                                                    SizedBox(
                                                      height: 0.4 *
                                                          SizeConfig
                                                              .heightMultiplier,
                                                    )
                                                  ],
                                                );
                                              }),
                                          GridView.builder(
                                            shrinkWrap: true,
                                            physics: ScrollPhysics(),
                                            itemCount: playListData.length,
                                            itemBuilder: (context, index) =>
                                                GridTile(
                                              child: Container(
                                                margin: EdgeInsets.all(5),
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10)),
                                                child: Stack(
                                                  alignment:
                                                      Alignment.bottomLeft,
                                                  children: [
                                                    Positioned(
                                                      left: 20.39 *
                                                          SizeConfig
                                                              .widthMultiplier,
                                                      bottom: 4.39 *
                                                          SizeConfig
                                                              .heightMultiplier,
                                                      child: Container(
                                                        decoration: new BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .all(Radius
                                                                        .circular(
                                                                            27))),
                                                        child: ClipRRect(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(27),
                                                          child:
                                                              CachedNetworkImage(
                                                            imageUrl: playListData[
                                                                    index]
                                                                .thirdimageURL,
                                                            fit: BoxFit.fill,
                                                            height: 26.76 *
                                                                SizeConfig
                                                                    .widthMultiplier,
                                                            width: 26.76 *
                                                                SizeConfig
                                                                    .widthMultiplier,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    Positioned(
                                                      left: 12.89 *
                                                          SizeConfig
                                                              .widthMultiplier,
                                                      bottom: 4.39 *
                                                          SizeConfig
                                                              .heightMultiplier,
                                                      child: Container(
                                                        decoration: new BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .all(Radius
                                                                        .circular(
                                                                            27))),
                                                        child: ClipRRect(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(27),
                                                          child:
                                                              CachedNetworkImage(
                                                            imageUrl: playListData[
                                                                    index]
                                                                .secondimageURL,
                                                            fit: BoxFit.fill,
                                                            height: 31.63 *
                                                                SizeConfig
                                                                    .widthMultiplier,
                                                            width: 31.63 *
                                                                SizeConfig
                                                                    .widthMultiplier,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    Positioned(
                                                      left: 3.64 *
                                                          SizeConfig
                                                              .widthMultiplier,
                                                      bottom: 4.39 *
                                                          SizeConfig
                                                              .heightMultiplier,
                                                      child: Container(
                                                        decoration: new BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .all(Radius
                                                                        .circular(
                                                                            27))),
                                                        child: ClipRRect(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(27),
                                                          child:
                                                              CachedNetworkImage(
                                                            fit: BoxFit.fill,
                                                            height: 36.49 *
                                                                SizeConfig
                                                                    .widthMultiplier,
                                                            width: 36.49 *
                                                                SizeConfig
                                                                    .widthMultiplier,
                                                            placeholder:
                                                                (context,
                                                                        url) =>
                                                                    Loading(),
                                                            imageUrl:
                                                                playListData[
                                                                        index]
                                                                    .topimageURL,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    Positioned(
                                                      left: 19.46 *
                                                          SizeConfig
                                                              .widthMultiplier,
                                                      child: new Text(
                                                        playListData[index]
                                                            .name,
                                                        style: new TextStyle(
                                                            fontFamily:
                                                                "IRANYekanMobile",
                                                            fontSize: 2.34 *
                                                                SizeConfig
                                                                    .textMultiplier,
                                                            color:
                                                                K_primaryWhite,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w400),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ),
                                            gridDelegate:
                                                SliverGridDelegateWithFixedCrossAxisCount(
                                                    crossAxisCount: 2),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              new Text('2'),
                              new Text('3'),
                              new Text('4'),
                            ]),
                      ),
                    ),
                  ],
                ),
              )),
        ),
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
