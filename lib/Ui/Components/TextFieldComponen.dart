import 'package:avapardaz_music/Theme/Consts.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_svg/svg.dart';

class TextFieldComponent extends StatefulWidget {
  final String attribute;
  final String hintText;
  final String labelText;
  final int maxLines;
  final TextInputAction textInputAction;
  final bool expands;
  final bool obscure;
  final bool requiredField;
  final bool showCursor;
  final Widget suffixIcon;
  final TextInputType keyboardType;
  final bool needChecking;
  //final Widget prefixIcon;
  final TextEditingController controller;
  final List<String Function(dynamic)> validators;

  TextFieldComponent({
    @required this.attribute,
    @required this.hintText,
    @required this.labelText,
    this.needChecking,
    this.textInputAction,
    this.keyboardType,
    this.suffixIcon,
    this.maxLines = 1,
    this.expands = false,
    this.obscure = false,
    this.showCursor = false,
    this.requiredField = false,
    this.validators = const <String Function(dynamic)>[],
    //this.prefixIcon,
    controller,
  }) : this.controller = controller ?? TextEditingController();

  @override
  _TextFieldComponentState createState() => _TextFieldComponentState();
}

class _TextFieldComponentState extends State<TextFieldComponent> {
  TextEditingController controller = new TextEditingController();
  bool showText = true;
  bool isValid = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(right: 10, bottom: 10),
          child: Align(
            alignment: Alignment.centerRight,
            child: Text(
              '${widget.labelText} ${widget.requiredField ? '*' : ''}',
              style: Theme.of(context).textTheme.caption.copyWith(
                  color: Color(0xffB4B4B4), fontWeight: FontWeight.w400),
              textScaleFactor: 1,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ),
        FormBuilderTextField(
          style: TextStyle(color: Colors.white),
          attribute: widget.attribute,
          cursorColor: Colors.white,
          textInputAction: widget.textInputAction,
          expands: widget.expands,
          keyboardType: widget.keyboardType,
          obscureText: widget.obscure ? showText : false,
          maxLines: widget.maxLines,
          showCursor: widget.showCursor,
          autovalidate: true,
          controller: controller,
          // maxLength: 4,
          //textAlign: TextAlign.center,
          onChanged: (value) {
            setState(() {
              isValid = FormBuilderValidators.validateValidators(
                          controller.text, widget.validators)
                      .toString() ==
                  "null";
            });
          },
          decoration: InputDecoration(
            prefixIcon: null,

            fillColor: K_accentBlack,
            filled: true,
            hintText: widget.hintText,
            hintStyle: TextStyle(color: K_accentblackLighter, fontSize: 22),
            // contentPadding: new EdgeInsets.only(right:MediaQuery.of(context).size.width/2-30),
            border: UnderlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide.none),
            errorBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.red, width: 2),
              borderRadius: BorderRadius.circular(10),
            ),
            errorStyle: Theme.of(context)
                .textTheme
                .overline
                .copyWith(color: Colors.red),
            floatingLabelBehavior: FloatingLabelBehavior.never,
          ),
          validators: widget.validators,
        ),
      ],
    );
  }
}
