import 'package:avapardaz_music/Blocs/PlayBloc.dart';
import 'package:avapardaz_music/NetworkUtils/Network_Utils.dart';
import 'package:avapardaz_music/Theme/Consts.dart';
import 'package:avapardaz_music/Theme/sizeConfig.dart';
import 'package:avapardaz_music/Ui/Components/Loading.dart';
import 'package:avapardaz_music/Ui/Components/MoreOptionsModel.dart';
import 'package:avapardaz_music/Ui/Components/SlidingUpPanelComponent.dart';
import 'package:avapardaz_music/Ui/Pages/NewMusicPlayer/MusicPlayerNew.dart';
import 'package:avapardaz_music/main.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

class HomeChannelCard extends StatefulWidget {
  const HomeChannelCard({Key key, @required this.index}) : super(key: key);

  @override
  _HomeChannelCardState createState() => _HomeChannelCardState();
  final int index;
}

class _HomeChannelCardState extends State<HomeChannelCard> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        if (await NetworkUtil.internal()
            .getChannelsMusic(channelsData[widget.index].id)) {
          Provider.of<PlayBloc>(context, listen: false)
              .addMediaItems(channelClickedSongs, true);
          print(queueBackGround.length);
          SlidingUpPanelComponent.open();
          NewMusicPlayer.audioPlayerButton();
        }

        setState(() {
          clicked = !clicked;
          clickedIndex = widget.index;
        });
      },
      child: Container(
        padding: EdgeInsets.all(2),
        decoration: BoxDecoration(
          color: channelsData[widget.index].bgColor,
          borderRadius: BorderRadius.circular(20),
        ),
        child: Stack(
          children: [
            Align(
              child: Container(
                padding: EdgeInsets.only(top: 5, right: 5),
                child: Text(
                  channelsData[widget.index].name,
                  style: TextStyle(
                      color: K_primaryWhite,
                      fontSize: 2.05 * SizeConfig.textMultiplier),
                ),
                margin: EdgeInsets.only(top: 4),
              ),
              alignment: Alignment.topCenter,
            ),
            Align(
              alignment: Alignment.topRight,
              child: GestureDetector(
                onTap: () {
                  showModalBottomSheet(
                      context: context,
                      builder: (builder) {
                        return MoreOptions();
                      });
                },
                child: Container(
                  padding: EdgeInsets.only(top: 5, right: 5),
                  child: Icon(
                    Icons.more_vert,
                    color: K_primaryWhite,
                    size: 20,
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                ),
                width: double.infinity,
                height: double.infinity,
                margin: EdgeInsets.only(top: 30),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: CachedNetworkImage(
                      placeholder: (context, url) => Align(
                            alignment: Alignment.center,
                            child: Loading(),
                          ),
                      imageUrl: channelsData[widget.index].coverImage,
                      fit: BoxFit.fill),
                ),
              ),
            ),
            Align(
              child: clicked
                  ? Container(
                      height: 50,
                      width: 50,
                      child: Lottie.asset(
                        'assets/Lottie/ChannelPalyAnimation.json',
                        fit: BoxFit.fill,
                      ),
                      // margin: EdgeInsets.only(top: 4),
                    )
                  : new Text(""),
              alignment: Alignment.center,
            ),
            Positioned(
                top: 30,
                right: 0,
                left: 0,
                bottom: 0,
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      gradient: LinearGradient(
                          colors: [Colors.transparent, Colors.black],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter)),
                )),
            Align(
              alignment: Alignment.bottomRight,
              child: GestureDetector(
                onTap: () {},
                child: IconButton(
                  icon: SvgPicture.asset(
                    'assets/images/like.svg',
                    semanticsLabel: 'like',
                    color: K_primaryWhite,
                    height: 24,
                    width: 24,
                  ),
                  onPressed: () {},
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
