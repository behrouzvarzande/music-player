
import 'package:avapardaz_music/Models/User.dart';
import 'package:avapardaz_music/Theme/Consts.dart';
import 'package:avapardaz_music/NetworkUtils/Network_Utils.dart';
import 'package:avapardaz_music/Ui/Components/SlidingUpPanelComponent.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';

class RootPage extends StatefulWidget {
  @override
  _RootPageState createState() => _RootPageState();
  static int selectedIndex = 0;
  static GlobalKey<NavigatorState> navigationKey = GlobalKey<NavigatorState>();
}

class _RootPageState extends State<RootPage> {
  Color libraryColor;

  ScrollController scrollController = new ScrollController();
  void _onItemTapped(int index) {
    if (index == 2 || index == 3) {
      if (User.accessToken != '') {
        getDataAPI();
        if (RootPage.navigationKey.currentState.canPop()) {
          RootPage.navigationKey.currentState.pop();
        }
        SlidingUpPanelComponent.close();

        setState(() {
          RootPage.selectedIndex = index;
          libraryColor = index == 2 ? K_primaryOrange : K_primaryWhite;
        });
      }else{
        Navigator.pushReplacementNamed(context,'/login');
      }
    } else {
      getDataAPI();
      if (RootPage.navigationKey.currentState.canPop()) {
        RootPage.navigationKey.currentState.pop();
      }
      SlidingUpPanelComponent.close();

      setState(() {
        RootPage.selectedIndex = index;
        libraryColor = index == 2 ? K_primaryOrange : K_primaryWhite;
      });
    }
  }

  getDataAPI() {
    NetworkUtil.internal().getSongs();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        // ignore: missing_return
        onWillPop: () {
          if (SlidingUpPanelComponent.panelController.isPanelOpen) {
            SlidingUpPanelComponent.panelController.close();
          } else {
            if (RootPage.navigationKey.currentState.canPop()) {
              RootPage.navigationKey.currentState.pop();
              return Future<bool>.value(false);
            }
            return Future<bool>.value(true);
          }
        },
        child: Scaffold(
          backgroundColor: K_primaryBlack,
          bottomNavigationBar: Theme(
            data: Theme.of(context).copyWith(canvasColor: K_accentBlack),
            child: BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              showUnselectedLabels: false,
              showSelectedLabels: false,
              unselectedItemColor: K_primaryWhite,
              items: <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(Icons.home),
                  title: Text('خانه'),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.search),
                  title: Text('جستجو'),
                ),
                BottomNavigationBarItem(
                  icon: SvgPicture.asset(
                    "assets/images/library.svg",
                    semanticsLabel: 'library',
                    color: libraryColor,
                    height: 16,
                    width: 16,
                  ),
                  title: Text('ترند'),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.person_outline),
                  title: Text('حساب کاربری'),
                ),
              ],
              backgroundColor: K_accentBlack,
              currentIndex: RootPage.selectedIndex,
              selectedItemColor: K_primaryOrange,
              onTap: _onItemTapped,
            ),
          ),
          body: GestureDetector(
              onTap: () => SlidingUpPanelComponent.panelController.open(),
              child: SlidingUpPanelComponent()),
        ));
  }
}
