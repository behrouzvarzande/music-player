// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Artist.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Artist _$ArtistFromJson(Map<String, dynamic> json) {
  return Artist(
    id: json['id'] as String,
    name: json['name'] as String,
    coverArt: json['avatar'] as String,
    bio: json['bio'] as String,
  )..songsCount = json['songsCount'] as int;
}

Map<String, dynamic> _$ArtistToJson(Artist instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'avatar': instance.coverArt,
      'songsCount': instance.songsCount,
      'bio': instance.bio,
    };
