import 'package:avapardaz_music/Models/Song.dart';
import 'package:flutter/material.dart';

class Channel {
  final String name;
  final String id;
  final Color bgColor;
  final String coverImage;
  List<Song> songs;
  int songsCount; // not im..
  Duration songsLength; // not implenet in server side

  Channel(
      {@required this.name,
      @required this.bgColor,
      @required this.id,
      @required this.coverImage});
  String get channelName => name;
  int get channelSongsCount => songsCount;
  Duration get channelSongsLength => songsLength;
  Color get channelBackgroundColor => bgColor;
  String get channelImage => coverImage;
//!Note : i bleive this methods are not useFull. we already have them in NetWorkUtils
  // factory Channel.fromJson(String str) => Channel.fromMap(json.decode(str));

  //String toJson() => json.encode(toMap());

  // factory Channel.fromMap(Map<String, dynamic> json) => Channel(
  //     name: json["name"] == null ? "" : json["name"],
  //     coverImage: json["coverArt"] == null ? "" : json["coverArt"],
  //     bgColor:
  //         json["bgColor"] == null ? Colors.white : fromHex(json["bgColor"]),
  //     txtColor: json["textColor"] == null
  //         ? Colors.black
  //         : fromHex(json['textColor']));

  // Map<String, dynamic> toMap() => {
  //       "name": name,
  //       "coverArt": imgUrl,
  //       "bgColor": '#${bgColor.value.toRadixString(16)}',
  //       "textColor": '#${txtColor.value.toRadixString(16)}',
  //     };
//!------------------------------------------------------------------------------------------
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }
}
