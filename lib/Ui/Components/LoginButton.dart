import 'package:avapardaz_music/Theme/Consts.dart';
import 'package:flutter/material.dart';

class LoginButton extends StatefulWidget {
  final String title;
  final double width;
  final double height;
  final double borderRadius;
  final Color backGroundColor;
  final TextStyle textStyle;
  final bool gradient;
  final Border border;
  final Function onPressed;
  final Widget icon;
  final EdgeInsetsGeometry padding;
  final List<BoxShadow> shadow;
  final TextDirection textDirection;
  bool active = true;
  LoginButton(
      {@required this.title,
      @required this.width,
      @required this.height,
      @required this.borderRadius,
      @required this.backGroundColor,
      @required this.textStyle,
      @required this.gradient,
      @required this.onPressed,
      this.border,
      @required this.icon,
      @required this.padding,
      this.shadow,
      @required this.textDirection,
      @required this.active});

  @override
  _CustomButtonState createState() => _CustomButtonState();
}

class _CustomButtonState extends State<LoginButton> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onPressed,
          child: Directionality(
        textDirection: widget.textDirection,
        child: Container(
          margin: widget.padding,
          width: widget.width,
          height: widget.height,
          decoration: new BoxDecoration(
              color: !widget.gradient
                  ? widget.active ? widget.backGroundColor : Colors.grey
                  : null,
              border: widget.active
                  ? (widget.border != null ? widget.border : null)
                  : Colors.transparent,
              gradient: widget.gradient
                  ? new LinearGradient(
                      begin: Alignment.centerRight,
                      end: Alignment.centerLeft,
                      stops: [
                          0.1,
                          0.5
                        ],
                      colors: [
                          K_DarkPurple,
                          K_LightPurpule,
                        ])
                  : null,
              borderRadius: BorderRadius.circular(widget.borderRadius),
              boxShadow: widget.active
                  ? (widget.shadow != null ? widget.shadow : null)
                  : null),
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            // mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: widget.textDirection == TextDirection.rtl
                    ? const EdgeInsets.only(right: 10)
                    : const EdgeInsets.only(right: 0),
                child: Opacity(
                    opacity: widget.textDirection == TextDirection.rtl ? 1 : 0,
                    child: widget.icon != null ? widget.icon : Text('')),
              ),
              Expanded(
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Text(
                      widget.title,
                      style: widget.active
                          ? widget.textStyle
                          : TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: widget.textDirection == TextDirection.rtl
                    ? const EdgeInsets.only(right: 0)
                    : const EdgeInsets.only(right: 10),
                child: Opacity(
                    opacity: widget.textDirection == TextDirection.rtl ? 0 : 1,
                    child: widget.icon != null ? widget.icon : Text('')),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
